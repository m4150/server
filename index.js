const express = require("express");
const cors = require('cors')
const mongoose = require("mongoose");
const app = express();
app.use(cors());
app.use(express.json())
const transactionRoute = require('./routes/transactionRoutes');
const typesRoute = require('./routes/typesRoute');
const sheetsRoute = require('./routes/sheetsRoute');
const usersRoute = require('./routes/usersRoute');
const userRoute = require('./routes/userRoute');
mongoose.connect('mongodb://localhost:27017/budgets?readPreference=primary&appname=MongoDB%20Compass&ssl=false', {
    useNewUrlParser: true
})
mongoose.connection.once('open', () => {
    console.log("Database ready");
})

// routes
app.use('/transactions', transactionRoute);
app.use('/types', typesRoute);
app.use('/sheets', sheetsRoute);
app.use('/users', usersRoute);
app.use('/user', userRoute);

// listen
app.listen(5000, () => console.log("App running on 5000"));