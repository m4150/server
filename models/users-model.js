const mongoose = require("mongoose");
const transactionType = require('./transaction-types');
const transactionTypeSchema = transactionType.transactionTypeSchema;
const sheets = require('./sheets-model')
const sheetsSchema = sheets.sheetSchema;
const sheetsModel = sheets.sheetModel;

const usersSchema = mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    transactionTypes: [transactionTypeSchema],
    sheets: [sheetsSchema],
    activeSheet: {
        type: String,
        ref: sheetsModel
    }
})

const usersModel = mongoose.model('users', usersSchema);
module.exports = usersModel;