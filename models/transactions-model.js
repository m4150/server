const mongoose = require('mongoose');
const transactionTypes = require('./transaction-types')
const transactionTypeModel = transactionTypes.transactionTypeModel;

const transactionSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    value: {
        type: Number,
        required: true
    },
    isExpense: {
        type: Boolean,
        required: true
    },
    time: {
        type: Date,
        required: true
    },
    category: {
        type: String,
        ref: transactionTypeModel,
    }
})

const transactionModel = mongoose.model('transactions', transactionSchema);

module.exports = {
    transactionSchema,
    transactionModel
};