const mongoose = require ('mongoose');

const transactionTypeSchema = mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    isExpense:{
        type: Boolean,
        required: true
    },
    isDefault:{
        type: Boolean,
        default: false,
    },
})

const transactionTypeModel = mongoose.model('transactionTypes', transactionTypeSchema);

module.exports = {
    transactionTypeSchema,
    transactionTypeModel
};