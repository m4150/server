const mongoose = require("mongoose");
const transactions = require('./transactions-model');
const transactionSchema = transactions.transactionSchema;

const sheetSchema = mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    time: {
        type: Date,
        required: true
    },
    transactions: {
        type: [transactionSchema]
    }
})

const sheetModel = mongoose.model('sheets', sheetSchema);

module.exports = {
    sheetSchema,
    sheetModel
}