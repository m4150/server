const transaction = require('../models/transactions-model');
const transactionModel = transaction.transactionModel;
const transactionTypes = require('../models/transaction-types');
const transactionTypeModel = transactionTypes.transactionTypeModel;

const viewAll = (req,res, next) => {
    transactionModel.find()
        // transaction.find().populate('category')
        .then(response => res.json(response))
        // .then(response => console.log(response))
        .catch(err => res.status(404).send(err))
}

const viewOutflows = (req,res, next) => {
    transactionModel.find({ isExpense: true }).populate('category')
    .then(response => res.json(response))
    .catch(err => res.status(404).send(err))
}

const viewInflows = (req,res, next) => {
    transactionModel.find({isExpense: false}).populate('category')
    .then(response => res.json(response))
    .catch(err => res.status(404).send(err))
}

const addTransaction = (req,res, next) => {
    let anyType;
    transactionTypeModel.findOne({isExpense: req.body.isExpense}, { name: "Any" })
    .then(r => {
        anyType = r._id
    })
    .catch(err => res.status(404).send(err))
    const Transaction = new transaction({
        name: req.body.name,
        value: req.body.value,
        isExpense: req.body.isExpense,
        category: req.body.transactionType ? req.body.transactionType : anyType,
        time: new Date(),
    })
    console.log(anyType, Transaction)
    Transaction.save()
    .then(response => 
        transactionModel.findById(response._id).populate('category')
        .then(r => res.send(r))
        .catch(err => res.status(404).send(err))
    )
    // .catch(err => res.status(404).send(err))
    .catch(err => console.log(err))
}

const modifyTransaction = (req,res, next) => {
    const update = {
        name: req.body.name,
        value: req.body.value,
        isExpense: req.body.isExpense,
        category: req.body.transactionType,
    }
    transactionModel.findOneAndUpdate({_id: req.body.id}, update, { new: true})
    .then(response => 
        transactionModel.findById(req.body.id).populate("category")
        .then(r => res.send(r))
        .catch(err => res.status(404).send(err))
    )
    .catch(err => res.status(404).send(err))
}

const remove = (req, res, next) => {
    transactionModel.findByIdAndRemove(req.body.id)
    .then(response => res.send(response))
    .catch(err => res.status(404).send(err))
}

module.exports = {
    viewAll, viewOutflows, viewInflows, addTransaction, remove, modifyTransaction
}