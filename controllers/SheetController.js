const sheets = require('../models/sheets-model')
const sheetsModal = sheets.sheetModel;
const usersModel = require('../models/users-model')

const addSheet = (req,res,next) => {
    const sheet = {
        name: req.body.name,
        time: new Date(),
    }
    usersModel.findOne({ _id: req.body.userID}, (err, user) => {
        if(err) res.status(404).send(err);
        else {
            user.sheets.push(sheet)
            user.save()
            .then( response => res.json(response.sheets))
            .catch(err => res.status(404).send(err))
        }
    })
}

const viewSheets = (req,res, next) => {
    usersModel.findOne({ name: req.query.username}, (err, user) => {
        if(err) res.status(404).send(err);
        else {
            res.json(user.sheets)
        }
    })
}

const activateSheet = (req,res, next) => {
    usersModel.findOne({ _id: req.body.userID}, (err, user) => {
        if(err) res.status(404).send(err);
        else {
            user.activeSheet = req.body.sheetID
            user.save()
            .then(res.json(user))
        }
    })
}

module.exports = {
    viewSheets, addSheet, activateSheet
}