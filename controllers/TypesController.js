const types = require('../models/transaction-types');
const typesModel = types.transactionTypeModel;
const userModel = require('../models/users-model')

const get = (req,res,next) => {
    userModel.findOne({username: req.body.username}, (err,user) => {
        if (err) console.log(err);
        else {
            if(user.transactionTypes.length === 0) {
                user.transactionTypes.push({
                    name: 'Any',
                    isExpense: false,
                    isDefault: true
                })
                user.transactionTypes.push({
                    name: 'Any',
                    isExpense: true,
                    isDefault: true
                })
                user.save()
                .then(response => res.json(response.transactionTypes))
                .catch(err2 => res.status(404).send(err2))
            } else res.json(user.transactionTypes)
        }
    })
}

const getInflows = (req, res, next) => {
    userModel.findOne({username: req.body.username}, (err,user) => {
        if (err) console.log(err);
        else {
            filteredInflowTypes = user.transactionTypes.filter(type => type.isExpense = false)
            res.send(filteredInflowTypes);
        }

    })
}

const getOutflows = (req, res, next) => {
    userModel.findOne({username: req.body.username}, (err,user) => {
        if (err) console.log(err);
        else {
            filteredInflowTypes = user.transactionTypes.filter(type => type.isExpense = true)
            res.send(filteredInflowTypes);
        }

    })
}
const addType = (req,res,next) => {
    const type = {
        name: req.body.name,
        isExpense: req.body.isExpense,
        isDefault: req.body.isDefault ? req.body.isDefault : false
    }
    userModel.findOne({_id: req.body.userID}, (err, user) => {
        if (err) console.log(err);
        else {
            user.transactionTypes.push(type);
            user.save()
            .then(resp => res.send(resp.transactionTypes))
        }
    })
}

const remove = (req, res, next) => {
    console.log(req.body.sheetID)
    userModel.findOne({_id: req.body.userID}, (err, user) => {
        if (err) console.log(err);
        else {
            let filteredSheet = user.sheets.map(sheet => console.log(sheet._id))
            console.log(filteredSheet.length)
        }
    })
}

// const remove = (req, res, next) => {
//     typesModel.findByIdAndDelete(req.params.id)
//     .then(response => res.send(response))
//     .catch(err => res.status(404).send(err))
// }

module.exports = { get, getInflows, getOutflows, addType, remove }