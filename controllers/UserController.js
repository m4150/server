const usersModel = require('../models/users-model')

const addUser = (req,res,next) => {
    const User = new usersModel({
        name: req.body.name,
    })
    User.save()
    .then(response => res.send(response))
    .catch(err => res.status(404).send(err))
}

const viewAll = (req,res, next) => {
    usersModel.find()
    .then(response => console.log("--o--" ,response))
    .catch(err => res.status(404).send(err))
}

const view = (req,res, next) => {
    usersModel.findOne({ name: req.query.username}, (err, user) => {
        if(err) res.status(404).send(err);
        else {
            res.send(user)
        }
    })
}

module.exports = {
    view, viewAll, addUser
}