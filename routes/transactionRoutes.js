const express = require('express');
const router = express.Router();

const transactionController = require('../controllers/TransactionController')

router.get('/', transactionController.viewAll);
router.get('/inflows', transactionController.viewInflows);
router.get('/outflows', transactionController.viewOutflows);
router.post('/add', transactionController.addTransaction);
router.put('/delete', transactionController.remove);
router.put('/modify', transactionController.modifyTransaction);

module.exports = router;