const express = require('express');
const router = express.Router();

const sheetController = require('../controllers/SheetController')

router.get('/', sheetController.viewSheets);
router.post('/add', sheetController.addSheet);
router.post('/activate', sheetController.activateSheet);

module.exports = router;