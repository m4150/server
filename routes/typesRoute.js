const express = require('express');
const router = express.Router();

const typesController = require('../controllers/TypesController')

router.get('/', typesController.get);
router.get('/inflows', typesController.getInflows);
router.get('/outflows', typesController.getOutflows);
// router.put('/delete/:id', typesController.remove);
router.put('/delete/:id', typesController.remove);
router.post('/add', typesController.addType);

module.exports = router;